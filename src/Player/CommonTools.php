<?php

namespace Player;

use GuzzleHttp\Client;

class CommonTools
{
    private static $redis;

    public static function limit($ip, $second, $times)
    {
        if (self::$redis == null) {
            self::$redis = new \Redis();
            try {
                self::$redis->pconnect('127.0.0.1');
            } catch (\Exception $e) {
                echo $e->getMessage();
            }
        }
        $key = md5($ip);
        $limit = self::$redis->get($key);
        if ($limit === false) {
            self::$redis->setEx($key, $second, 1);
            return true;
        } else if ($limit >= $times) {
            return false;
        } else {
            self::$redis->incr($key);
            return true;
        }
    }

    public static function auth($server, $ua, $ip)
    {
        $auth_client = new Client([
            'headers' => [
                'User-Agent' => $ua
            ]
        ]);
        $server_resp = $auth_client->get($server, [
            'query' => ['ip' => $ip]
        ]);
        $valid = json_decode($server_resp->getBody());
        if (!$valid->success) {
            return false;
        } else {
            return true;
        }
    }
}